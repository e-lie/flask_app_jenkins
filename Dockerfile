FROM python:3.7

LABEL maintainer="Elie Gavoty"
LABEL version="1.0"

# Ne pas lancer les app en root dans docker
RUN useradd microblog
WORKDIR /home/microblog

#Ajouter tout le contexte sauf le contenu de .dockerignore
ADD . .

# Installer les déps python, pas besoin de venv car docker
RUN pip install -r requirements.txt
RUN chmod a+x app.py test.py && \
    chown -R microblog:microblog ./

# Déclarer la config de l'app
ENV FLASK_APP app.py
EXPOSE 5000

# Changer d'user pour lancer l'app
USER microblog

CMD ["./app.py"]